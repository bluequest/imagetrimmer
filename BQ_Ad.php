<?php

class BQ_Ad {
    
    var $id;
    var $name;  
    var $format_dir; 
    var $platform; 
    var $responsive; 
    var $creative_dir;
    var $crossDevice;  
    var $views = [];

    function __construct($format_object) {

        //Get the easier stuff
        $this->id = $format_object->id;
        $this->name = $format_object->name;
        $this->format_dir = $format_object->format_dir;
        $this->responsive = $format_object->responsive;
        $this->crossDevice = $format_object->crossDevice;

        $this->creative_dir = $_SERVER['DOCUMENT_ROOT'] . "creative";

    }

    //CONSTRUCTOR FOR WHEN CREATING OBJECT FROM UPLOADED FILES/DATA
    public static function constructFromUpload($format_object, $post, $files) {

        $instance = new static($format_object);

        $instance->populateFromUpload($format_object, $post, $files);

        $instance->createAd();

        return $instance;

    }

    //CONSTRUCTOR FOR WHEN CREATING OBJECT FROM SAVED JSON OBJECT
    public static function contructFromObject($object) {

        $instance = new static($object);

        $instance->populateFromObject($object);

        // $instance->createAd();

        return $instance;
    }

    function populateFromUpload($format_object, $post, $files) {

        //Get the platform directory
        foreach ($format_object->platforms as $key => $platform) {
            if($platform->id == $post['platform']) {
                $this->platform = $platform;    
                break;
            }
        }

        //Get the views
        foreach ($format_object->views as $key => $view) {
            $this->views[$view->name] = BQ_View::constructFromUpload($view->name, $files[$view->name . '_PSD'], $format_object, $platform); // add views to object, using the name to get the uploaded files
        }
    }

    function populateFromObject($object) {

        $this->platform = $object->platform;

        //Get the views
        foreach ($object->views as $key => $view) {
            $this->views[$view->name] = BQ_View::constructFromObject($view); // add views to 
        }

    }

    function getHTML() {
        $html = '';
        foreach ($this->views as $view) {
            $html = $html . $view->html;
        }
        return $html;
    }

    function getCSS() {
        $css = '';
        foreach ($this->views as $view) {
            $css = $css . $this->getCSSRecursive($view, $view->name);
        }
        return $css;
    }

    function getCSSRecursive($container, $viewName) {
        $css = '';
        if (property_exists($container, 'elements')) {
            foreach ($container->elements as $key => $element) {
                $css = $css . "\n#$viewName .{$element->name} {" . $element->css . "}";
                $css = $css . $this->getCSSRecursive($element, $viewName);
            }            
        }
        return $css;
    }

    function saveImages() {
        foreach ($this->views as $view) {
            foreach ($view->elements as $element) {
                $element->saveImage($this->creative_dir);
            }
        }
    }

    function createAd() {
        $html = $this->getHTML();
        $css = $this->getCSS();
        
        $this->copyFiles("format/{$this->format_dir}", $html, $css);
        $this->copyFiles("format/{$this->format_dir}/platform/{$this->platform->dir}", $html, $css);

        //copy the base js for the right format and platform
        // copy("format/{$this->format_dir}/platform/{$this->platform->dir}/core.js", "{$this->creative_dir}/core.js");

        $new_dir = $this->creative_dir . ($this->platform->flat ? '' : '/style') . "/custom.css";
        if (!file_exists(dirname($new_dir))) {
            mkdir(dirname($new_dir), 0777, true);
        }
        file_put_contents ($new_dir, $css);
        
    }

    function copyFiles($dir, $html, $css) {

        $format_files = glob("{$dir}/*.{php,css,js,html,jpg,png}", GLOB_BRACE);      
        $dirs = glob($dir."/*", GLOB_ONLYDIR);

        // COPY THE FILES
        if (count($format_files) > 0) {
            foreach($format_files as $format_file) {

                if(pathinfo($format_file)['basename'] == "index.php") {
                    // use output buffering to stop the include being rendered directly to the page. Instead store the input as a varible $content
                    ob_start();
                    include $format_file;
                    $content = ob_get_clean();
                    file_put_contents ($this->creative_dir . "/index.html" , $content);

                } elseif (pathinfo($format_file)['basename'] == "scale_controller.php") {
                    
                    // use output buffering to stop the include being rendered directly to the page. Instead store the input as a varible $content
                    ob_start();
                    include $format_file;
                    $content = ob_get_clean();
                    // ob_end_flush();


                    //get the target directory
                    $new_dir = $this->creative_dir . ($this->platform->flat ? '' : '/js');

                    //create the directory if it doesn't exist
                    if (!$this->platform->flat && !file_exists($new_dir)) {
                        mkdir($new_dir, 0777, true);
                    }


                    file_put_contents ($new_dir . "/scale_controller.js" , $content);

                } elseif (pathinfo($format_file)['basename'] == "base.php") {
                    
                    // use output buffering to stop the include being rendered directly to the page. Instead store the input as a varible $content
                    ob_start();
                    include $format_file;
                    $content = ob_get_clean();
                    // ob_end_flush();


                    //get the target directory
                    $new_dir = $this->creative_dir . ($this->platform->flat ? '' : '/style');

                    //create the directory if it doesn't exist
                    if (!$this->platform->flat && !file_exists($new_dir)) {
                        mkdir($new_dir, 0777, true);
                    }


                    file_put_contents ($new_dir . "/base.css" , $content);

                } else {
                    if(strpos($format_file, 'platform') == false || strpos($format_file, "platform/{$this->platform->dir}") !== false) {
                        $new_dir = str_replace("platform/{$this->platform->dir}/", '', str_replace("format/{$this->format_dir}", $this->creative_dir, $format_file));

                        //create the directory if it doesn't exist
                        if (!file_exists(dirname($new_dir))) {
                            mkdir(dirname($new_dir), 0777, true);
                        }

                        //copy the file into the directory
                        copy($format_file, $new_dir);                        
                    }
                }
            }
        }

        //GO TO ANY SUBFOLDERS
        if (count($dirs) > 0) {
            foreach($dirs as $sub_dir) {
                if(basename($sub_dir) != "platform"){
                    $this->copyFiles($sub_dir, $html, $css);
                }
            }
        } 
    }

    // PUBLIC METHODS

    function update($files, $view_name) {
        $this->views[$view_name]->update($files[$view_name . '_files'], $this->platform);
        $this->createAd();
    }

    function deleteElement($elem_name, $view_name) {

        $element = $this->views[$view_name]->elements[$elem_name];

        //remove the element from the ad object and delete the saved image
        $this->deleteImage($element, $view_name);
        unset($this->views[$view_name]->elements[$elem_name]);

        // reconstruct the ad
        $this->views[$view_name]->createHTML();
        $this->createAd();
    }

    function deleteImage($element, $view_name) {

        // if it's an image delete it, if not drill down recusively through the divs until we find all the nested images
        if($element->type == 'image') {
            unlink($this->creative_dir . ($this->platform->flat? '' : "/images/{$view_name}") . '/' . $element->name . '.' . $element->ext);
        } else {
            foreach ($element->elements as $key => $child) {
                $this->deleteImage($child, $view_name);
            }
        }
    }

    function changeElementOrigin($elem_name, $origin, $view_name) {
        $view = $this->views[$view_name];
        $element = $view->elements[$elem_name];
        $element->origin = $origin;
        $element->createCSS($view);
        $element->createHTML($view);                    
        $view->createHTML();
        $this->createAd();
    }

    function groupElements($element_names, $group_name, $view_name) {
        $this->views[$view_name]->createGroup($element_names, $group_name);
        $this->createAd();        
    }

    function breakGroup($group_name, $view_name) {
        $this->views[$view_name]->breakGroup($group_name);
        $this->createAd();        
    }

    function moveElement($elementNames, $view_name) {
        $this->views[$view_name]->moveElement($elementNames);
        $this->createAd();
    }

    function changeScaling($elem_name, $scaleBy, $view_name) {
        $view = $this->views[$view_name];
        $element = $view->elements[$elem_name];
        $element->scaleBy = $scaleBy;
        $element->createHTML($view);                    
        $view->createHTML();
        $this->createAd();
    }

}

?>