var Scale_Controller = function() {

	var me = this,
	views = {
<?php
	foreach ($this->views as $key => $view) {
		echo "\n\t\t" . $view->name . ": {\n\t\t\twidth: " . $view->width . ",\n\t\t\theight: " . $view->height . "\n\t\t},";
	}
?>
		
		rotate: {
			width: 1000,
			height: 640
		}
	},
	scaled_objects = [],
	currentView = null,
	designSize = null;

	//create the view change event
	var eViewChange = new Event('viewChange');

	//Event Listeners

	function getScaledElements() {
        var scaledElements = document.querySelectorAll('[data-scale]');
		for (var i = scaledElements.length - 1; i >= 0; i--) {
			addElement(scaledElements[i]);
		}
		resize_handler();
		window.addEventListener("resize", resize_handler);
	}

	function addElement(element) {
		var elementStyle = window.getComputedStyle(element);
		var newElement = {
			width: elementStyle.getPropertyValue('width').replace('px', ''),
			height: elementStyle.getPropertyValue('height').replace('px', ''),
			element: element,
			scaleBy: element.getAttribute('data-scale')
		}
		scaled_objects.push(newElement);
	}

	function resize_handler() {

		designSize = getDesignSize();
		if(designSize != currentView) {
			currentView = designSize;

			for (var name in views)
			{
				if(views[name] == currentView) {
					document.getElementById(name).style.display = "block";
					load_view_images(name);
					// get the name of the view;
					eViewChange.view = name;
				} else {
					document.getElementById(name).style.display = "none";
					pauseVideos(name);
				}
			}

			document.dispatchEvent(eViewChange);

		}		

		scale();

	}

	function getDesignSize() {
		if (window.innerHeight < window.innerWidth && window.innerHeight <= 500) {
			return views['rotate'];
		} else if (window.innerWidth <= 500) {
			return views['mobile'];
		} else if (window.innerWidth / window.innerHeight < 1.1) {
			return views['tablet'];
		} else {
			return views['desktop'];
		}
	}

	function load_view_images(view_id) {
		var images = document.querySelectorAll('#' + view_id + ' img');
		// var images = $('#' + view_id + ' img');
		for (var i = images.length - 1; i >= 0; i--) {
			if(!images[i].hasAttribute('src') && images[i].hasAttribute('data-src')) {
				images[i].setAttribute("src", images[i].getAttribute("data-src"));
			}
		}

	}

	function scale() {
		var ratio = getRatios();
		for (var i = scaled_objects.length - 1; i >= 0; i--) {
			//resize
			scaled_objects[i].element.style.width = (scaled_objects[i].width * ratio[scaled_objects[i].scaleBy]) + "px";
			scaled_objects[i].element.style.height = (scaled_objects[i].height * ratio[scaled_objects[i].scaleBy]) + "px";
		}
	}

	function getRatios(){
		var ratio = {};
		if((window.innerWidth / window.innerHeight) < (designSize.width / designSize.height)) {
			ratio.min = window.innerWidth / designSize.width;
			ratio.max = window.innerHeight / designSize.height;
		} else {
			ratio.min = window.innerHeight / designSize.height;
			ratio.max = window.innerWidth / designSize.width;
		}
		ratio.width = window.innerWidth / designSize.width;
		ratio.height = window.innerHeight / designSize.height;
		return ratio;
	}

	function pauseVideos(view) {		
		var videos = document.getElementById(view).getElementsByTagName("video");
		for (var i = videos.length - 1; i >= 0; i--) {
			videos[i].pause();
		}
	}

	// public functions
	this.addElement = function(element) {
		addElement(element);
		resize_handler();
	}

	//initiator
	getScaledElements();

}

document.addEventListener("DOMContentLoaded", function() {
	var scaleController = new Scale_Controller();
});
