var Animator = new function() {

    var currentview,
    engineFire = null;

    this.viewChange = function(view) {
        if (view != currentview) {
            currentview = view        
            // this.startAnim();
        }
    }

    this.startAnim = function() {
        var tl = new TimelineLite();
        tl.set("#stage", { opacity: 0})
        .to($('#stage'), 1, {opacity:1});
    };

    this.fadeOut = function($elem, remove) {
        var tl = new TimelineLite();
        tl.to($elem, 1, { opacity: 0 });
        if(remove) {
            tl.set($elem, {display: 'none'});
        }
        
    }

    this.showPage = function($elem) {
        this.fadeIn($elem, function() {
            animating = false;
        });
        if($elem.hasClass('page_2')) {
            this.revealEngineButton();
        } else if($elem.hasClass('page_4')) {
            this.revealDashButton();
        }
    }

    this.fadeIn = function($elem, callback) {
        var tl = new TimelineLite();
        tl.set($elem, {display: 'block'})
        .set($elem, {opacity: 0})
        .to($elem, 1, { opacity: 1 });
        if($.isFunction(callback)) {
            tl.eventCallback("onComplete", callback);
        }
    }

    this.revealEngineButton = function() {
        var tl = new TimelineLite();
        if(currentview == 'mobile_view') {
            tl.from($('#' + currentview + ' .engine_btn_group'), 0.5, {opacity: 0}, 0.5);
        } else {
            tl.from($('#' + currentview + ' .line_mask'), 0.8, {width: 0}, 0.5)
            .from($('#' + currentview + ' .engine_btn'), 0.5, {opacity: 0}, "label")
            .from($('#' + currentview + ' .engine_text'), 0.5, {opacity: 0}, "label");            
        }
    }

    // this.startEngine = function() {
    //     this.engineBtnPress();


    //     //delay the animation by 1 second
    //     setTimeout(function() {
    //         engineFire = new TimelineMax();
    //         var $fire = $('#' + currentview + ' .fire');

    //         console.log($fire);
    //         engineFire.to($fire, 0.02, {opacity: 1})
    //         .to($fire, 0.02, {opacity: 0})            
    //         .to($fire, 0.02, {opacity: 1}, "+=0.1")
    //         .to($fire, 0.02, {opacity: 0})
    //         .to($fire, 0.02, {opacity: 1})
    //         .to($fire, 0.02, {opacity: 0})
    //         .to($fire, 0.02, {opacity: 1}, "+=0.3")
    //         .to($fire, 0.02, {opacity: 0})
    //         .to($fire, 0.02, {opacity: 1}, "+=0.2")
    //         .to($fire, 0.02, {opacity: 0}).repeat(-1);
    //     }, 1000);
    // }

    // this.stopEngine = function() {
    //     if(engineFire != null) {
    //         engineFire.stop();
    //         $('.fire').css({opacity: 0});
    //     }
    // }

    this.engineBtnPress = function() {
        var tl = new TimelineMax();
        tl.to($('#' + currentview + ' .engine_btn'), 0.15, {scale: 0.9, x: '-50%', y: '-50%'})
        .to($('#' + currentview + ' .engine_btn'), 0.15, {scale: 1, x: '-50%', y: '-50%'});
    }

    this.revealDashButton = function() {
        var tl = new TimelineLite();
        tl.from($('#' + currentview + ' .dash_line_mask'), 0.8, {width: 0}, 0.5)
        .from($('#' + currentview + ' .dash_control'), 0.5, {opacity: 0}, "label");
    }

}




