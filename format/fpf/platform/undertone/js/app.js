/* jslint browser: true */
/*global $:true, window:true */


// Prevent scrolling on touch devices. (Stops navigation bar hiding and creative resizing)
function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

var elementsHidden = [];
var engineSound;
var page = 1;
var currentView = 'desktop_view';
var seq_d = null,
    seq_t = null,
    seq_m = null;

var clickHandler = function(e) {

    var target = e.target;

    if (!$(target).hasClass('clickable') && !(target).hasAttribute("data-player-id")) {

        stopEvent(e);

        // hide the element and add it to an array so that we can remember to show it again.
        target.style.display = "none";
        elementsHidden.push(target);

        //Manually fire the event for desired underlying element
        var BottomElement = document.elementFromPoint(e.clientX, e.clientY);
        $(BottomElement).click();

        return false;

    } else {
        // once we have arrived at something clickable. Show all the elements we have had to hide in order to drill down
        for (var i = elementsHidden.length - 1; i >= 0; i--) {
            elementsHidden[i].style.display = "block";
        }
        elementsHidden = [];

        if (target.hasAttribute('data-id')) {
            stopEvent(e);
            $('#' + target.getAttribute('data-id')).click();
        }

        //stopt the background from firing as a clicktag
        if (target.getAttribute('id') === 'BACKGROUND') {
            stopEvent(e);
        }

    }
};

function stopEvent(e) {
    e = e || window.event;
    if (!e.preventDefault) { //IE quirks
        e.returnValue = false;
        e.cancelBubble = true;
    }
    e.preventDefault();
    e.stopPropagation();
}



// store references to DOM elements and optimize selectors for performance purposes
// for more info: http://24ways.org/2011/your-jquery-now-with-less-suck/
var openButton = $('#ut_open'),
    closeButton = $('#ut_close'),
    theBody = $('body');

window.startAd = function() {
    // creative.min.js binds window.startAd() to #ut_open 'click',
    // as well as platform ready

    engineSound = new Audio("audio/R8_motor.mp3");
    attachEventListeners();

    // playView();

};

window.closeAd = function() {
    // creative.min.js binds window.closeAd() to autoclose timer expiration (15s)
    // but **NOT** when the #ut_close is clicked. This means that you must bind it yourself,
    // which allows you to modify timing and coordinate animations.
    engineStop();
    theBody.removeClass('opened');
    theBody.addClass('closed');
};

// clicking the openButton should restore the ad to an expanded state
// but may entail different steps than the initial open animation
openButton.click(function() {
    // window.startAd is called by default by the format code
    // in order to resize its container on the pub page (height 500px || full mobile device height)
});

// if the close button is clicked, present the leavebehind state
// note that closing the ad may interrupt an animation,
// leaving the ad in an unfinished state.
// Re-opening the ad should restore things to a presentable state.
closeButton.click(function() {
    // window.closeAd is called by default by the format code
    // in order to resize its container on the pub page (height 90px || 50px on mobile)
    window.closeAd();
});

function attachEventListeners() {

    //core functionality
    window.addEventListener("touchmove", function(e) {
        stopEvent(e);
    }, true);

    if (window.addEventListener) {
        window.addEventListener('click', clickHandler, true);
    } else {
        window.attachEvent('onclick', clickHandler);
    }

    // Navigation between pages
    $navBtns = $('.nav').on('click', function() {
        var newPage = $(this).attr("data-page");
        if (animating == false && newPage != page) {
            changePage(newPage);
        }
    });

    // pressing of the engine button (p2)
    $("#ENGINE_BTN").on('click', function() {
        if (!engineOn) {
            engineStart();
        } else {
            engineStop();
        }
        Animator.engineBtnPress();
    });

    // Hangle arrow pressing on p3 (360)
    $('#LEFT_ARROW').on('click', function() {
        carousel('.angle', 'left', 1, 5, 'angle');
    });
    $('#RIGHT_ARROW').on('click', function() {
        carousel('.angle', 'right', 1, 5, 'angle');
    });

    //Handle swiping on page 3 (360)
    var touchstart = 0;
    $('.bg_angle').on("touchstart", function(e) {
        touchstart = e.touches[0].clientX;
    }, true);
    $('.bg_angle').on("touchend", function(e) {
        var touchEnd = e.changedTouches[0].clientX;
        if (touchEnd - touchstart > 20) {
            carousel('.angle', 'right', 1, 5, 'angle');
            undertone.creative.trackEvent('SWIPE', 'RIGHT');
        } else if (touchEnd - touchstart < -20) {
            carousel('.angle', 'left', 1, 5, 'angle');
            undertone.creative.trackEvent('SWIPE', 'LEFT');
        }
    }, true);

    engineSound.onended = function() {
        engineStop();
    }

    $('#COCKPIT_LEFT').on('click', function() {
        carousel('.dash_', 'left', 1, 3, 'dash');
    });

    $('#COCKPIT_RIGHT').on('click', function() {
        carousel('.dash_', 'right', 1, 3, 'dash');
    });

}

var animating = false;
var engineOn = false;

function changePage(newPage) {
    animating = true;
    Animator.fadeOut($(".nav[data-page*='" + page + "']"));
    Animator.fadeIn($(".nav[data-page*='" + newPage + "']"));
    engineStop();
    Animator.fadeOut($(".page_" + page), true);
    page = newPage;
    Animator.showPage($(".page_" + page));      
    if (newPage == 1) {
        setTimeout(panLights, 100);
        // panLights();
    }
}

function panLights() {
    switch (currentView) {
        case 'desktop_view':
            seq_d.canvas.style.display = 'block';
            seq_d.playAnim(41, 54, "8fps", false, false, false, function() {
                seq_d.canvas.style.display = 'none';
            });
            break;
        case 'tablet_view':
            seq_t.canvas.style.display = 'block';
            seq_t.playAnim(41, 54, "8fps", false, false, false, function() {
                seq_t.canvas.style.display = 'none';
            });
            break;
        case 'mobile_view':
            seq_m.canvas.style.display = 'block';
            seq_m.playAnim(41, 54, "8fps", false, false, false, function() {
                seq_m.canvas.style.display = 'none';
            });
            break;
    }
}

window.angle = 1;
window.dash = 1;

function carousel(name, direction, first, last, current) {

    console.log('carousel');

    $(name + window[current]).css({ 'opacity': 0 });
    if (direction == "left") {
        if (window[current] == first) {
            window[current] = last;
        } else {
            window[current] -= 1;
        }
    } else if (direction == "right") {
        if (window[current] == last) {
            window[current] = first;
        } else {
            window[current] += 1;
        }
    }
    $(name + window[current]).css({ 'opacity': 1 });
}

function cacheImages(images) {
    for (var i = images.length - 1; i >= 0; i--) {
        var img = new Image();
        img.src = 'img/' + images[i];

    }
}


function engineStart() {
    engineSound.src = engineSound.src;
    engineSound.play();
    engineOn = true;
}

function engineStop() {
    if (engineSound) engineSound.pause();
    engineOn = false;
}


function changeView(view) {
    currentView = view;
    engineStop();

    Animator.viewChange(currentView);
    // changePage(1);
    playView();
}

function playView() {

    $('#stage').hide();

    if (seq_d) seq_d.stopAnim();
    if (seq_t) seq_t.stopAnim();
    if (seq_m) seq_m.stopAnim();

    if (page != 1) {
        engineStop();
        $(".nav[data-page*='" + page + "']").css({ opacity: 0 });
        $(".nav[data-page*='1']").css({ opacity: 1 });

        $(".page_" + page).hide();
        $(".page_1").show();
        page = 1;
    }

    switch (currentView) {
        case 'desktop_view':
            if (!seq_d) {
                seq_d = new imgSeq(0, 54, "img/desktop/seq/r8_undertone_desk_[].jpg", 5, true, "sequence_d", seqCacheComplete);
            } else {
                seq_d.canvas.style.display = 'block';
                $('#stage').show();
            }
            hideInterface_d();
            seq_d.playAnim(0, 54, "8fps", false, false, false, function() {
                seq_d.canvas.style.display = 'none';
            }, {
                func: showInterface_d,
                frame: 40
            });
            break;
        case 'tablet_view':
            if (!seq_t) {
                seq_t = new imgSeq(0, 54, "img/tablet/seq/r8_undertone_tab_[].jpg", 5, true, "sequence_t", seqCacheComplete);
            } else {
                seq_t.canvas.style.display = 'block';
                $('#stage').show();
            }
            hideInterface_t();
            seq_t.playAnim(0, 54, "8fps", false, false, false, function() {
                seq_t.canvas.style.display = 'none';
            }, {
                func: showInterface_t,
                frame: 40
            });
            break;
        case 'mobile_view':
            if (!seq_m) {
                seq_m = new imgSeq(0, 54, "img/mobile/seq/r8_undertone_mob_[].jpg", 5, true, "sequence_m", seqCacheComplete);
            } else {
                seq_m.canvas.style.display = 'block';
                $('#stage').show();
            }
            hideInterface_m();
            seq_m.playAnim(0, 54, "8fps", false, false, false, function() {
                seq_m.canvas.style.display = 'none';
            }, {
                func: showInterface_m,
                frame: 40
            });
            break;
        case 'rotate_view':
            if (undertone.creative.pgxsmp) undertone.creative.pgxsmp();
            $('#stage').show();
            break;
    }
}

function seqCacheComplete(seq) {
    seq.canvas.setAttribute('data-scale', 'max');
    scaleController.addElement(seq.canvas);
    // important!! - This fires the start of the ad. from this point it will show on the page
    if (undertone.creative.pgxsmp) undertone.creative.pgxsmp();
    $('#stage').show();
}

function hideInterface_d() {
    $('.text_1_d, .logo_sport_d, .desktop_group_1, .desktop_group_4, .desktop_group_5').hide();
}

function showInterface_d() {
    Animator.fadeIn($('.text_1_d, .logo_sport_d, .desktop_group_1, .desktop_group_4, .desktop_group_5'));
}

function hideInterface_t() {
    $('.text_1_t, .logo_sport_t, .logo_t, .tablet_group_1, .tablet_group_2, .tablet_group_3').hide();
}

function showInterface_t() {
    Animator.fadeIn($('.text_1_t, .logo_sport_t, .logo_t, .tablet_group_1, .tablet_group_2, .tablet_group_3'));
}

function hideInterface_m() {
    $('.text_1, .logo_sport, .logo, .mobile_group_2, .mobile_group_3, .mobile_group_5').hide();
}

function showInterface_m() {
    Animator.fadeIn($('.text_1, .logo_sport, .logo, .mobile_group_2, .mobile_group_3, .mobile_group_5'));
}
