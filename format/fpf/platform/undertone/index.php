<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]>        <html class="no-js lt-ie10 ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <title>Reveal | Undertone</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href='style.css'>
    <link rel="stylesheet" type="text/css" href='style/custom.css'>
    <!-- media query and HTML5 elements polyfill for IE8 support -->
    <!--[if lt IE 9]>
        <script type="text/javascript" src="js/respond.min.js"></script>
        <script>
            document.createElement('header');
            document.createElement('nav');
            document.createElement('section');
            document.createElement('article');
            document.createElement('aside');
            document.createElement('footer');
            document.createElement('hgroup');
        </script>
    <![endif]-->
</head>

<body>
    <div class="stage" id="stage">
<?php
    foreach ($this->views as $key => $view) {
        echo "\n\t\t" . $view->html;
    }
?>
        <div id='rotate' class='view'></div>
    </div>
    <script id="ut_creativeLib">
    // This detects whether the browser requires the Zepto or jQuery library
    document.write('<script src=js/' +
        ('__proto__' in {} ? 'zepto' : 'jquery') +
        '.min.js><\/script>');
    </script>
    <script type="text/javascript" src="js/TweenMax.min.js"></script>
    <script type="text/javascript" src="js/creative.min.js"></script>
    <script type="text/javascript" src="js/modules.js"></script>
    <script type="text/javascript" src="js/scale_controller.js"></script>
    <script type="text/javascript" src="js/animator.js"></script>
    <script type="text/javascript" src="js/imgSeq.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</body>

</html>
