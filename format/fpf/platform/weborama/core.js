window.onload = init;

function init() {

    screenad.setAlignment('left', 'top');
    screenad.setSize("100%", "100%");
    screenad.setSticky(true);
    screenad.setZIndex(1);
    screenad.hideNavBar();
    screenad.setBlockPageTouches(true);
    screenad.position();

    // make sure that the ad is ready
    setTimeout(adStart, 500); // start the ad animation

}
var body;

function adStart() {
    console.log("adStarts");
    setTimeout(autoClose, 14500);

    body = document.getElementsByTagName("body")[0];
    attachClickTags();
    addEventListeners();
}

function attachClickTags() {
    document.getElementById("container").addEventListener("click", function() {
        event.stopPropagation();
        screenad.event("background_exit");
        screenad.click("default");
    });
}

function addEventListeners() {
    // document.getElementById("close").addEventListener("click", close);
}

function autoClose() {
    setTimeout(close, 500);
}

function close() {
    if (event) {
        event.stopPropagation();
    }
    screenad.command("close");
}

