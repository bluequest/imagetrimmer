<!DOCTYPE html>
<html>

<head>
    <!-- ADRIME SCREENAD META DATA (don't edit/remove) -->
    <!-- SCRVERSION: screenad_interface_1.0.3 -->
    <!-- SCRFORMAT: layer -->
    <!-- SCRWIDTH: 100% -->
    <!-- SCRHEIGHT: 100% -->    

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">    
    <link rel="stylesheet" href="base.css" />
    <link rel="stylesheet" href="custom.css" />
    <script type='text/javascript' src='http://media.adrcdn.com/scripts/screenad_interface_1.0.3_scrambled.js'></script>
    <script type="text/javascript" src="scale_controller.js"></script>  
    <script src="core.js"></script>
    <script src="creative.js"></script>    
</head>

<body>     

    <div id="container" >

        <?php
            foreach ($this->views as $key => $view) {
                echo "\n\t\t" . $view->html;
            }
        ?>

    </div>
</body>

</html>

