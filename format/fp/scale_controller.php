var Scale_Controller = function() {

    var me = this,
    views = {
<?php
    foreach ($this->views as $key => $view) {
        echo "\t\t" . $view->name . ": {\n\t\t\twidth: " . $view->width . ",\n\t\t\theight: " . $view->height . "\n\t\t}";
    }
?>

    },
    scaled_objects = [],
    designSize = null;

    function getScaledElements() {
        var scaledElements = document.querySelectorAll('[data-scale]');
        for (var i = scaledElements.length - 1; i >= 0; i--) {
            addElement(scaledElements[i]);
        }
        designSize = views.default; // there is only one view, the default view 
        resize_handler();
        window.addEventListener("resize", resize_handler);
    }

    function addElement(element) {
        var elementStyle = window.getComputedStyle(element);
        var newElement = {
            width: elementStyle.getPropertyValue('width').replace('px', ''),
            height: elementStyle.getPropertyValue('height').replace('px', ''),
            element: element,
            scaleBy: element.getAttribute('data-scale')
        }
        scaled_objects.push(newElement);
    }

    function resize_handler() {   

        scale();

    }

    function scale() {
        var ratio = getRatios();
        for (var i = scaled_objects.length - 1; i >= 0; i--) {
            //resize
            scaled_objects[i].element.style.width = (scaled_objects[i].width * ratio[scaled_objects[i].scaleBy]) + "px";
            scaled_objects[i].element.style.height = (scaled_objects[i].height * ratio[scaled_objects[i].scaleBy]) + "px";
        }
    }

    function getRatios(){
        var ratio = {};
        if((window.innerWidth / window.innerHeight) < (designSize.width / designSize.height)) {
            ratio.min = window.innerWidth / designSize.width;
            ratio.max = window.innerHeight / designSize.height;
        } else {
            ratio.min = window.innerHeight / designSize.height;
            ratio.max = window.innerWidth / designSize.width;
        }
        ratio.width = window.innerWidth / designSize.width;
        ratio.height = window.innerHeight / designSize.height;
        return ratio;
    }

    // public functions
    this.addElement = function(element) {
        addElement(element);
        resize_handler();
    }

    //initiator
    getScaledElements();

}


document.addEventListener("DOMContentLoaded", function() {
    var scaleController = new Scale_Controller();
});

