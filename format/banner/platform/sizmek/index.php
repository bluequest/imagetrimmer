<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>MPU</title>
    <link rel="stylesheet" href="style/base.css" />
    <link rel="stylesheet" href="style/custom.css" />
    <script src="scripts/EBLoader.js"></script>
    <!--[if lte IE 9]><script src="scripts/classList.js"></script><![endif]-->
    <script src="script/creative.js"></script>
    <script src="script/core.js"></script>
</head>

<body class="p1">
    <?php
        foreach ($this->views as $key => $view) {
            echo "\n\t\t" . $view->html;
        }
    ?>
</body>

</html>





