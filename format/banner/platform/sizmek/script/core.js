var isIOS = (/iPhone|iPad|iPod/i).test(navigator.userAgent);
var isIE = navigator.userAgent.indexOf("Trident/") > -1;
console.log(isIE);
var isIE9 = (/MSIE 9\./i).test(navigator.userAgent);
var isIE10 = (/MSIE 10\./i).test(navigator.userAgent);
var isFireFox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

function initEB() {

    var bq_preview = window.location.search.substring(1).split("=")[1];

    if (bq_preview == "true") {
        startAd();
        return;
    }

    if (!EB.isInitialized()) {
        EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd);
    } else {
        startAd();
    }

}

function startAd() {
    document.getElementById("ad").addEventListener("click", function() {
       EB.clickthrough();
    });
    creative.play();
}

function stopEvent(e) {
    if (e) {
        if (isIE9 || isIE10) {
            e.cancelBubble = true;
        }  else {
            e.stopPropagation();
        }
    }
}

//Start the whole thing
window.addEventListener("load", initEB);



