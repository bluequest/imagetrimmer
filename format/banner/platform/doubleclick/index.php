<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php 
        echo "\t<meta name='ad.size' content='width={$this->views['banner']->width},height={$this->views['banner']->height}'>\n"
    ?>
    <link rel="stylesheet" href="base.css" />
    <link rel="stylesheet" href="custom.css" />
    <title>Banner</title>
    <script type="text/javascript" src="https://s0.2mdn.net/ads/studio/Enabler.js"></script>
    <script type="text/javascript" src="jquery-3.0.0.min.js"></script>
    <script type="text/javascript" src="TweenMax.min.js"></script>
    <script type="text/javascript" src="script.js"></script>
</head>

<body>
    <?php
        foreach ($this->views as $key => $view) {
            echo "\n\t\t" . $view->html;
        }
    ?>
</body>

</html>





