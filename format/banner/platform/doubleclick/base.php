body {
    margin: 0px;
    padding: 0;
    overflow: hidden;
    cursor: pointer;
    -moz-user-select: -moz-none;
    -khtml-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
body > div {
    overflow: hidden;
}
img, div {
    position: absolute;
}
<?php
    foreach ($this->views as $key => $view) {
        echo "\n#{$key} {";
        echo "\n\twidth: " . $this->views['banner']->width . 'px;';
        echo "\n\theight: " . $this->views['banner']->height . 'px;';
        echo "\n\tdisplay: none;";
        echo "\n}";
    }
?>
