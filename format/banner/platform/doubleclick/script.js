/**
 *  Main onload handler
 */
window.addEventListener('load', preInit);

/**
 * Window onload handler.
 */
function preInit() {

    if (Enabler.isInitialized()) {
        init();
    } else {
        Enabler.addEventListener(
            studio.events.StudioEvent.INIT,
            init
        );
    }
}

/**
 * Ad initialisation.
 */
function init() {

    attachEvents();

    // Polite loading
    if (Enabler.isPageLoaded()) {
        adReady();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, adReady);
    }
}




// ---------------------------------------------------------------------------------
// MAIN
// ---------------------------------------------------------------------------------


function adReady() {
	show(); //show the ad content
	// setTimeout(Animator.play, 2000); FOR EXAMPLE
}

function show() {
    document.getElementById("banner").style.display = "block";
}


function attachEvents() {

    //CLICKTAGS
    // document.getElementById("banner").addEventListener("click", function() {
    //     Enabler.exit("background_exit");
    // });
    // document.getElementsByClassName("logo")[0].addEventListener("click", function(e) {
    //     Enabler.exit("logo_exit");
    //     e.stopPropagation();
    //     e.returnValue = false;
    // });
    // document.getElementsByClassName("cta")[0].addEventListener("click", function(e) {
    //     Enabler.exit("cta");
    //     e.stopPropagation();
    //     e.returnValue = false;
    // });
}

var Animator = new function() {
	var timeline = new TimelineLite();

    // EXAMPLE
	// this.play = function() {
	// 	timeline.set($('.text_1, .text_2, .text_3, .text_4, .cta'), {y: '10px'}, "start")

	// 	//move background and fade in shadow
	// 	.to($('.bg'), 0.8, {x: '50px'}, "start")
	// 	.to($('.shadow'), 0.8, {opacity: 1}, "start+=0.3")

	// 	// fade first text in and out
	// 	.to($('.text_1'), 0.5, {y: 0, opacity: 1}, "start+=1")
	// 	.to($('.text_1'), 0.5, {y: '-10px', opacity: 0}, "start+=5")

	// 	// fade 2nd text in and out
	// 	.to($('.text_2'), 0.5, {y: 0, opacity: 1}, "start+=6")
	// 	.to($('.text_2'), 0.5, {y: '-10px', opacity: 0}, "start+=10")

	// 	// fade 3rd text in and out
	// 	.to($('.text_3'), 0.5, {y: 0, opacity: 1}, "start+=11")
	// 	.to($('.text_3'), 0.5, {y: '-10px', opacity: 0}, "start+=15")

	// 	// fade 4th text + cta in
	// 	.to($('.text_4'), 0.5, {y: 0, opacity: 1}, "start+=16")
	// 	.to($('.cta'), 0.5, {y: 0, opacity: 1, display: 'block'}, "start+=16.5");

	// }

}