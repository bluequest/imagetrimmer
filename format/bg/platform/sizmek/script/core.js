console.log("HELLO?");

var adDiv;

var isIOS = (/iPhone|iPad|iPod/i).test(navigator.userAgent);
var isIE = navigator.userAgent.indexOf("Trident/") > -1;
console.log(isIE);
var isIE9 = (/MSIE 9\./i).test(navigator.userAgent);
var isIE10 = (/MSIE 10\./i).test(navigator.userAgent);
var isFireFox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1

var expandTime;
var collapseTimer;

var fifteen;

var hasVideo = false; // set to false if there is no video
var Video_1; // define video. This can be array for multiple videos

function initEB() {

    var bq_preview = window.location.search.substring(1).split("=")[1];

    if (bq_preview == "true") {
        startAd();
        return;
    }

    if (!EB.isInitialized()) {
        EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd);
    } else {
        startAd();
    }

}

function startAd() {
    adDiv = document.getElementById("ad");

    if(hasVideo) { // if the ad has a video set it up
        Video_1 = new Video(); //video.js class
    }

    useInAppCloseButton();
    addEventListeners();

}

function useInAppCloseButton() {
    var sdkData = EB.getSDKData();

    if (sdkData !== null) {
        if (sdkData.SDKType === "MRAID") {
            // set sdk to use custom close button
            EB.setExpandProperties({
                useCustomClose: true
            });
        }
    }
}

function addEventListeners() {
    // expand functionality
    document.getElementById("banner").addEventListener("mouseover", preExpand);
    document.getElementById("banner").addEventListener("mouseout", clearExpand);
    document.getElementById("banner").addEventListener("click", function() {
        clearExpand();
        expand();
    });
    
    document.getElementById("blueRight").addEventListener("transitionend", function() {
        whiteLeft.className = "semi-circle atBack"
    });
    document.getElementById("blueLeft").addEventListener("transitionend", expand);

    //collapse functionality

    if(document.getElementById("close")) document.getElementById("close").addEventListener("click", collapse);

    document.getElementById("panel").addEventListener("mouseleave", function() {
        if(adDiv.classList.contains("expanded")){
            collapseTimer = setTimeout(collapse, 3000);
        }
    });
    document.getElementById("panel").addEventListener("mouseenter", function () {
        clearTimeout(collapseTimer);
    });

}

//EXPAND
function preExpand() {
    if (!isIE9) {
        loadingCircle();
    } else {
        expandTime = setTimeout(expand, 1000)         
    }
}
function clearExpand(){
    if (!isIE9) {
        clearLoadCircle();
    } else {
        clearTimeout(expandTime);
    } 
}
function expand() {
    EB.expand();
    adDiv.classList.remove("collapsed");
    adDiv.classList.add("expanded");

    //Start 15 second tracking timer
    fifteen = setTimeout(function() {
        EB.userActionCounter("15second_Panel_Open");
    }, 15000);
}

function loadingCircle(){
    var circle = document.getElementById("loading_circle");
    var blueLeft = document.getElementById("blueLeft");
    var blueRight = document.getElementById("blueRight");
    var whiteLeft = document.getElementById("whiteLeft");
    circle.className = "show"
    blueLeft.className = "semi-circle blue360"
    blueRight.className = "semi-circle blue180"

}
function clearLoadCircle(){
    var circle = document.getElementById("loading_circle");
    var blueLeft = document.getElementById("blueLeft");
    var blueRight = document.getElementById("blueRight");
    var whiteLeft = document.getElementById("whiteLeft");
    circle.className = ""
    blueLeft.className = "semi-circle"
    blueRight.className = "semi-circle"
    whiteLeft.className = "semi-circle"
}

//COLLAPSE
function collapse(event, track) { //track is an optional variable defaulting to true
    EB.collapse();
    stopEvent(event);
    adDiv.classList.remove("expanded");
    adDiv.classList.add("collapsed");
    if (hasVideo) Video_1.stopVideo(); // video.js
    clearTimeout(fifteen);
    // if(track == null || track == true) {
    //     EB.userActionCounter("Collapse Banner");
    //     console.log("Collapse Banner");
    // }
    return;
}


//TOOLS
function addClickTag(id, tag, stopProp, useCapture) {  // function (id of element, clicktag, boolean - stop the event propigating)

    if (!stopProp) stopProp = true; //default stopProp to true
    if (!useCapture) useCapture = false; //default stopProp to true

    console.log(id);
    document.getElementById(id).addEventListener("click", function(event) {

        if (stopProp) {
            event.stopPropagation();
        }

        // if (stopProp) stopEvent(event);

        switch(tag) {
            case 1:
                EB.clickthrough("background");
                console.log("background");
                break;
            case 2:
                EB.clickthrough("cta");
                console.log("cta");
                break;
            case 3:
                EB.clickthrough("logo");
                console.log("logo");
                break;
            case 4: //custom clicktag functionality!

                var controlsHeight = (isIE) ? 60 : 30;

                var controls_y = this.offsetHeight - controlsHeight;
                var relPos_y = event.pageY - document.getElementById("video-container").offsetTop;

                if (relPos_y < controls_y){
                    EB.clickthrough("video");
                    console.log("video");                        
                } else {
                    return;                        
                }

                break;
            default:
                console.log("click tag attach failed");
                break;
        }

        if (adDiv.className.indexOf("expanded") > -1) {
            collapse(event, false);
        }        

        return;

    }, useCapture);
}

function stopEvent(e) {
    if (e) {
        if (isIE9 || isIE10) {
            e.cancelBubble = true;
        }  else {
            e.stopPropagation();
        }
    }
}



//Start the whole thing
window.addEventListener("load", initEB);



