function Video() {

	var me = this;

	console.log("New Video");

	var videoContainer = document.getElementById("video-container");
	var video = document.getElementById("vid1");
	var sdkVideoPlayer = document.getElementById("sdk-video-player");
	var sdkVideoPlayButton = document.getElementById("sdk-video-play-button");

	var videoTrackingModule;

	var playButton = document.getElementById("play_button");
	var wallpaper = document.getElementById("wallpaper");

    initVideo();
    attachVideoEvents();

    function initVideo() { //set the video up as a Sizmek tracked video
	    var sdkData = EB.getSDKData();
	    var useSDKVideoPlayer = false;
	    var sdkPlayerVideoFormat = "mp4"; // or use "webm" for the webm format

	    if (sdkData !== null) {
	        if (sdkData.SDKType === "MRAID" && sdkData.version > 1) {
	            document.body.classList.add("sdk");

	            // set sdk to use custom close button
	            EB.setExpandProperties({
	                useCustomClose: true
	            });

	            var sourceTags = video.getElementsByTagName("source");
	            var videoSource = "";

	            for (var i = 0; i < sourceTags.length; i++) {
	                if (sourceTags[i].getAttribute("type")) {
	                    if (sourceTags[i].getAttribute("type").toLowerCase() === "video/" + sdkPlayerVideoFormat) {
	                        videoSource = sourceTags[i].getAttribute("src");
	                    }
	                }
	            }

	            videoContainer.removeChild(video);
	            video = null;

	            sdkVideoPlayButton.addEventListener("click", function() {
	                if (videoSource !== "") {
	                    EB.playVideoOnNativePlayer(videoSource);
	                }
	            });

	            useSDKVideoPlayer = true;
	        }
	    }

	    if (!useSDKVideoPlayer) {
	        videoContainer.removeChild(sdkVideoPlayer);
	        videoTrackingModule = new EBG.VideoModule(video);
	    }
	}

	function attachVideoEvents() {
		//hide play button on play
		video.addEventListener("play", function () {
            playButton.style.display = "none";
            wallpaper.style.display = "none";
            // console.log("add controls");
            // video.setAttribute("controls", "controls");
        });

		//SHow play button on pause
        video.addEventListener("pause", function () { 
            playButton.style.display = "block";
        });

        video.addEventListener("ended", function () {
            playButton.style.display = "block";        	
            wallpaper.style.display = "block";
            // video.removeAttribute("controls");
        });

        playButton.addEventListener("click", playVideo);
        wallpaper.addEventListener("click", playVideo);
        
	}


	function videoEndedHandler() {
	    resetCollapse();	//core.js
	    me.stopVideo();
	}

	function playVideo(event) {
		stopEvent(event);

        playButton.style.display = "none";
        wallpaper.style.display = "none";
		video.play();		
	}

	function pauseVideo(){
		video.pause();
	}

	//external functions
	this.playVideo = function() {
		playVideo();
	} 

	this.replayVideo = function() {
		if (video.currentTime > 0) {
			video.currentTime = 0;
		}
		me.playVideo();
	} 

	this.stopVideo = function() {
		video.pause();
		video.currentTime = 0;
		videoTrackingModule._started = false;
		videoTrackingModule._25played = false;
		videoTrackingModule._50played = false;
		videoTrackingModule._75played = false;
		videoTrackingModule._ended = false;
	}

	this.videoPaused = function() {
	    return video.paused;
	}

	//custom video


}


