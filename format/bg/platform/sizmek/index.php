<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Brand Gallery</title>
    <link rel="stylesheet" href="style/base.css" />
    <link rel="stylesheet" href="style/custom.css" />
    <script>
    	EBModulesToLoad = ['Video', 'EBCMD'];
    </script>
    <script src="script/EBLoader.js"></script>
    <!--[if lte IE 9]><script src="scripts/classList.js"></script><![endif]-->
    <script>
    	EB.initExpansionParams(600, 0, 900, 600);
    </script>
    <script src="script/video.js"></script>
    <script src="script/creative.js"></script>
    <script src="script/core.js"></script>
</head>

<body class="p1">
    <div id="container" class="versionB">
        <div id="ad" class="collapsed">
<?php
    foreach ($this->views as $key => $view) {
        echo "\n\t\t" . $view->html;
    }
?>
        </div>
    </div>
</body>

</html>





