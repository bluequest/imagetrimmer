 // var Photo = React.createClass({

//   toggleLiked: function() {
//     this.setState({
//       liked: !this.state.liked
//     });
//   },

//   getInitialState: function() {
//     return {
//       liked: false
//     };
//   },

//   render: function() {
//     var buttonClass = this.state.liked ? 'active' : '';

//     return (
//       <div className='photo'>
//         <img src={this.props.src} />

//         <div className='bar'>
//           <button onClick={this.toggleLiked} className={buttonClass}>
//             ♥
//           </button>
//           <span>{this.props.caption}</span>
//         </div>
//       </div>
//     );
//   }
// });

// var PhotoGallery = React.createClass({

//   render: function() {

//     var photos = this.props.photos.map(function(photo) {
//       return <Photo src={photo.url} caption={photo.caption} />
//     });

//     return (
//       <div className='photo-gallery'>
//         {photos}
//       </div>
//     );
//   }
// });






var i = 0;

var reloadScaleControler = function() {

	console.log("reloadScaleControler");
	var scriptTag = document.getElementById("scale_script");

	if(!scriptTag) {
		scriptTag = document.createElement('script');
		scriptTag.setAttribute("id", "scale_script");
		scriptTag.setAttribute("src", "creative/scale_controller.js");
		document.body.appendChild(scriptTag);
	} else {
		scriptTag.setAttribute("src", "creative/scale_controller.js?v=" + i);
	}
	
	i++;

}

var viewURL = "getJSON.php";

var View = React.createClass({
	getInitialState: function() {
		return {
			selectElementKey: 0,
			data: undefined
		}
	},
	getData: function() {
		console.log("get data");
		// console.log(this.props.url);
		$.ajax({
	      url: this.props.url,
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	      	// console.log("success");
	        this.setState({data: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	      	// console.log("failure");
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	},
	clickImgHander: function (elem) {
		// console.log(elem.props.propsKey);
		if (elem.props.selected) {
			this.setState({
				selectElementKey: 0
			})
		} else {
			this.setState({
				selectElementKey: elem.props.propsKey
			})			
		}
	},
	// life cycle
	componentDidMount: function() {
		this.getData();
	},
	componentDidUpdate: function() {
		reloadScaleControler();
	},
	getDomElements: function() {
		var clickImgHander = this.clickImgHander;
		var selectElementKey = this.state.selectElementKey;
		var domElements = this.state.data.map(function(domElement) {

			// check if the element is already selected
			var selected = false;
			if (domElement.id === selectElementKey) {
				selected = true;
			}

	      	return <DOMElement src={domElement.src} name={domElement.name} cssStyle={domElement.cssStyle} clickEvent={clickImgHander} selected={selected} propsKey={domElement.id} />

	    });
	    return domElements;
	},
	render: function() {
		// console.log(this.state.data);
		var domElements = this.state.data ? this.getDomElements() : undefined;

		// console.log("render");
		return(
			<div className="view" id={this.state.data ? this.state.data.name : null}>
				{domElements}
			</div>
		);
	}
});

var DOMElement = React.createClass({
	clickHander: function() {
		this.props.clickEvent(this);
	},
	render: function() {

		// console.log(this.props.selected);
		var containerClass = this.props.selected ? "selected" : ""
		var elSrc = "creative/" + this.props.src;
		var elClass = this.props.src + " scale";
		return(
			<div className={containerClass}>
				<img src={elSrc} id={this.props.name} className="scale" style={this.props.cssStyle} onClick={this.clickHander} />
				<div className="panel"></div>
			</div> 
		);
	}
});

React.render(<View url={viewURL} />, document.getElementById("wrapper"));



