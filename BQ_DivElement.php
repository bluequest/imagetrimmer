<?php

class BQ_DivElement extends Bq_Element {

    var $responsive;

    var $elements = array();

    function __construct() {

        $this->type = 'div';

    }

    public static function constructNew($elements, $container, $name) {

        $instance = new static();

        $instance->getGeometry($elements, $container);

        // $instance->origin = [1, 1];

        $instance->name = $name;
        $instance->responsive = $container->responsive;

        $instance->addChildren($elements);

        $instance->createHTML($container);
        $instance->createCSS($container);

        return $instance;

    }

    public static function reconstruct($element) {

        $instance = new static();
        
        $instance->name = $element->name;

        $instance->top = $element->top;
        $instance->left = $element->left;
        $instance->right = $element->right;
        $instance->bottom = $element->bottom;
        $instance->width = $element->width;
        $instance->height = $element->height;

        $instance->origin = $element->origin;
        $instance->scaleBy = $element->scaleBy;

        $instance->css = $element->css;
        $instance->html = $element->html;

        $instance->responsive = $element->responsive;

        foreach ($element->elements as $key => $childElement) {
            if($childElement->type == 'image') {
                $instance->elements[$key] = BQ_ImgElement::constructFromObject($childElement);
            } elseif ($childElement->type == 'div') {
                $instance->elements[$key] = BQ_DivElement::reconstruct($childElement);
            }
        }

        return $instance;
        
    }

    // Loops through the (to-be) child elements and determined the geometry of the div element
    function getGeometry($elements, $container) {

        // Start with all the variables at 0. NB this may not be the best way to do this.
        $top = null;
        $left = null;
        $right = null;
        $bottom = null;


        foreach ($elements as $element) {       

            if($top === null || $element->top < $top) {
                $top = $element->top;
            }
            if($left === null || $element->left < $left) {
                $left = $element->left;
            }
            if($right === null || $element->right < $right) {
                $right = $element->right;
            }
            if($bottom === null || $element->bottom < $bottom) {
                $bottom = $element->bottom;
            }
        }

        // Now save all of these to the object.
        $this->top = $top;
        $this->left = $left;
        $this->right = $right;
        $this->bottom = $bottom;

        //Work out he height and width
        $this->width = $container->width - ($this->left + $this->right);
        $this->height = $container->height - ($this->top + $this->bottom);

    }

    function addChildren($elements) {
        foreach ($elements as $key => $element) {

            //Change the elements positioning to reflect the fact that it is now inside a div
            $element->top = $element->top - $this->top;
            $element->left = $element->left - $this->left;
            $element->right = $element->right - $this->right;
            $element->bottom = $element->bottom - $this->bottom;

            //re-write the elements css
            $element->createCSS($this);

            //Add element
            $this->elements[$key] = $element;
        }
    }

    function createHTML($container) {  
        $html = "<div name='{$this->name}' class='{$this->name}"
            .($container->responsive ? ' ' . $this->origin[0] . $this->origin[1] : '') . " group'"            
            .($container->responsive ? " data-scale='{$this->scaleBy}'" : "")
            .">";
        foreach ($this->elements as $key => $element) {
            $html = $html . $element->html;
        }
        $html = $html . "</div>";
        $this->html = $html;
    }

    // here we overwrite the parent class' create static css function, so that we can give the divs (groups) width and height
    function createCSS_Static() {
        $this->css = "left: {$this->left}px; top: {$this->top}px; width: {$this->width}px; height: {$this->height}px;";
    }

}

?>