<?php

class BQ_View {

    var $name;
    var $format_id;

    var $width;
    var $height;

    var $html;

    var $responsive;
    var $crossDevice;

    var $elements = array();


    function __construct($name, $width, $height, $responsive, $crossDevice, $format_id){
        $this->name = strtolower($name);
        $this->format_id = $format_id;
        $this->width = intval($width);
        $this->height = intval($height);
        $this->responsive = $responsive;
        $this->crossDevice = $crossDevice;
    }

    public static function constructFromUpload($name, $psd, $format_object, $platform) {

        $instance = new static($name, 0, 0, $format_object->responsive, $format_object->crossDevice, $format_object->id);

        $instance->createElementsFromPSD($psd, $platform);

        $instance->createHTML();

        return $instance;
    }

    public static function constructFromObject($view_object) {

        $instance = new static($view_object->name, $view_object->width, $view_object->height, $view_object->responsive, $view_object->crossDevice, $view_object->format_id);

        $instance->html = $view_object->html;

        $instance->createElementsFromObject($view_object);

        return $instance;
    }


    function createElementsFromPSD($psd, $platform) {

        if(file_exists($psd['tmp_name']) && is_uploaded_file($psd['tmp_name'])) {

            $psd = new Imagick($psd["tmp_name"]);

            $psd->setIteratorIndex(0); 
            $image_page = $psd->getImagePage();
            $this->width = $image_page['width'];
            $this->height = $image_page['height'];

            $num_layers = $psd->getNumberImages();

            //NB this is starting from the second layer. the first layer seems to the 'stage' itself. Above we get the dimensions from that
            for ($i=1; $i<$num_layers; $i++) {
                $psd->setIteratorIndex($i); 
                $this->elements[$this->getElementNamePSD($psd)] = BQ_ImgElement::constructFromUpload($psd, $this, $platform, true);
            }
        }
    }

    function createElementsFromFiles($files, $platform) {
        $reArrayedFiles = $this->reArrayFiles($files);

        foreach ($reArrayedFiles as $file) {
            if(!empty($file['name'])) {
                if($file['type'] == 'image/png') {
                    $this->elements[$this->getElementNameIMG($file)] = BQ_ImgElement::constructFromUpload($file, $this, $platform, false);
                } else if($file['type'] == 'image/vnd.adobe.photoshop') {
                    $this->createElementsFromPSD($file, $platform);
                }
            }
        }
    }

    function createElementsFromObject($view_object) {
        foreach ($view_object->elements as $key => $element) {
            if($element->type == 'image') {
                $this->elements[$key] = BQ_ImgElement::constructFromObject($element);
            } elseif ($element->type == 'div') {
                $this->elements[$key] = BQ_DivElement::reconstruct($element);
            }
        }
    }

    function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }

    function getElementNamePSD($layer) {
        $name = str_replace("_jpg", "", $layer->getImageProperty('label'));
        return $name;
    }

    function getElementNameIMG($file) {
        $file = str_replace("_jpg.png", ".jpg", $file['name']);
        return explode('.', $file)[0];
    }

    function processImages() {
        foreach ($this->elements as $element) {
            $element->processImage();
        }
    }

    function createHTML() {

        // 1. Reset View HTML
        $this->html = "\n<div id='{$this->name}' class='view'>";

        //if this is BG need to ad the loading circle within the banner. This is not great, needs to be improved
        if($this->name == 'banner' && $this->format_id == 3) { //format_id 3 = Brand Gallery
            $this->html = $this->html . "\n\t<div id='loading_circle'>";
            $this->html = $this->html . "\n\t\t<div id='blueLeft' class='semi-circle'></div>";
            $this->html = $this->html . "\n\t\t<div id='blueRight' class='semi-circle'></div>";
            $this->html = $this->html . "\n\t\t<div id='whiteLeft' class='semi-circle'></div>";
            $this->html = $this->html . "\n\t\t<div id='whiteRight' class='semi-circle'></div>";
            $this->html = $this->html . "\n\t</div>";
        }      


        // 2.1 Populate with BQ_ImgElements
        foreach ($this->elements as $element) {
            $this->html = $this->html . "\n\t" . $element->html;
        }

        //3. Close View div
        $this->html = $this->html . "\n</div>";
    }

    function createGroup($element_names, $group_name) {

        //collect the elements in a new array and remove from the view's elements array
        $elements_to_group = array();
        foreach ($element_names as $element_name) {
            $elements_to_group[$element_name] = $this->elements[$element_name];
            unset($this->elements[$element_name]);      
        }

        //Add the group to the view's elements
        $this->elements[$group_name] = BQ_DivElement::constructNew($elements_to_group, $this, $group_name);
        $this->createHTML();
    }

    function breakGroup($group_name) {
        $group = $this->elements[$group_name];

        //Get the groups elements and put them directy in to the views list
        foreach ($group->elements as $key => $element) {
            $element->top = $element->top + $group->top;
            $element->left = $element->left + $group->left;
            $element->right = $element->right + $group->right;
            $element->bottom = $element->bottom + $group->bottom;

            //re-write the elements css
            $element->createCSS($this);

            //Add element
            $this->elements[$key] = $element;
        }

        unset($this->elements[$group_name]);

        $this->createHTML();

    }

    function update($files, $platform) {
        $this->createElementsFromFiles($files, $platform);        
        $this->createHTML();
    }

    function moveElement($elementNames) {

        $sorting_array = $this->elements;

        $i = 0;
        foreach ($elementNames as $name) {
            $sorting_array[$name] = array('sort' => ++$i, 'val' => $this->elements[$name]);
        }
        
        // b1. sort the array by the sorting order
        uasort($sorting_array, function($a, $b) { return $a['sort'] > $b['sort']; });

        //a2. reset the value to it's actual value, getting rid of the sorting order
        foreach($sorting_array as &$val) $val = $val['val'];

        $this->elements = $sorting_array;

        $this->createHTML();

    }

}

?>