document.addEventListener('DOMContentLoaded', setupEditor);

var selectedElements = [];
var hiddenElements = [];
var ad;
var view = null;
var controller;
var group_name;
var edits = 0;
var menuPinned = false;
var fileForm, fileInput, errorMsg;
var droppedFiles = false;
var iframe;
var iframe_document;
var originButtons;
var isDown;
var displayClass = null;

function setupEditor() {

    setVars();
    attachEditorEvents();
    getAd();

}

function setVars() {

    controller = document.getElementById("controller");
    group_name = document.getElementById("group_name");
    fileForm = $('.fileForm');
    fileInput = fileForm.find('input[type="file"]');
    errorMsg = fileForm.find('.form_error span');

    iframe = document.getElementsByTagName('iframe')[0];
    iframe_document;
    originButtons = document.getElementById("origin").getElementsByTagName('button');

}

function attachEditorEvents() {

    // Check to see if mouse button is down
    $(document).mousedown(function() {
        isDown = true;    
    })
    .mouseup(function() {
        isDown = false;
        displayClass = null;
    });

    // Attach iframe resize functionality
    iframe.style.width = (window.innerWidth - 180) + 'px';
    window.onresize = function(event) {
        iframe.style.width = (window.innerWidth - 180) + 'px';
    };

    // Attach delete funcitonality
    document.getElementById("delete").addEventListener('click', deleteElement);

    $(document).keydown( function(event){
        if(event.keyCode == 46) deleteElement();
    })


    // Attach Group functionality
    document.getElementById("group").addEventListener('click', groupElements);
    group_name.addEventListener('input', validateGroupName);
    group_name.addEventListener('focus', validateGroupName);

    // Attach Break functionality
    document.getElementById("break").addEventListener('click', breakGroup);

    // Attach Change Origin functionality
    for (var i = originButtons.length - 1; i >= 0; i--) {
        originButtons[i].addEventListener('click', changeOrigin);
    }

    // Attach Scale functionality
    $('#scale select').on('change', scaleElement);

    // Attach Sort functionality
    $("#layers").sortable({
        stop: orderElement,
        cancel: '.display_btn' 
    });

    // allow touch events to be treated as click events
    initTouchEvents();

    // add iframe onload event
    iframe.addEventListener('load', iframeLoadHandler);

}

// Treats touch events as if they were click events
function initTouchEvents() {
    document.addEventListener("touchstart", touchHandler, true);
    document.addEventListener("touchmove", touchHandler, true);
    document.addEventListener("touchend", touchHandler, true);
    document.addEventListener("touchcancel", touchHandler, true);
}

function touchHandler(event) {
    var touch = event.changedTouches[0];

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent({
            touchstart: "mousedown",
            touchmove: "mousemove",
            touchend: "mouseup"
        }[event.type], true, true, window, 1,
        touch.screenX, touch.screenY,
        touch.clientX, touch.clientY, false,
        false, false, false, 0, null);

    touch.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}

// *EDIT* - we already have the iframe 
function attachFileDragEvents() {
    $("iframe").contents().find("body").bind("dragenter", function() {
        fileForm.addClass('is-dragover');
    });

    fileForm.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
            e.preventDefault();
            e.stopPropagation();
        })
        .on('dragover dragenter', function() {
            fileForm.addClass('is-dragover');
        })
        .on('dragleave dragend drop', function() {
            fileForm.removeClass('is-dragover');
        })
        .on('drop', function(e) {
            droppedFiles = e.originalEvent.dataTransfer.files;
            uploadFile();
        });

}

function createLayerList() {

    console.log(view);

    var layers = ad.views[view].elements;
    var html = '';

    for (var propertyName in layers) {
        var layer = layers[propertyName];
        var index = hiddenElements.indexOf(layer.name);
        var displayIcon = (index < 0)?  "ui-icon-bullet" : "ui-icon-radio-off"
        html += "<li id='" + layer.name + "'><span id='"+ layer.name+"_display' class='display_btn ui-icon "+displayIcon+"'></span>" + layer.name + "</li>"
    }
    $('#layers').html(html);

    attachLayersClicks();
}



function getViewFromDOM() {
    return $("iframe").contents().find(".view:visible").attr('id');
}

function viewChangeHandler(e) {
    view = e.view;

    //repoulate the layer list
    setViewName();
    createLayerList();

    // de-select any selected elements
    for (var i = selectedElements.length - 1; i >= 0; i--) {
        unhighlightElement(selectedElements[i]);
    }
    selectedElements = [];
    hiddenElements = [];

}

function load_iframe() {
    edits += 1;
    iframe.setAttribute("src", "../creative/index.html?v=" + edits);
}

function iframeLoadHandler() {

    fileForm.removeClass('is-uploading');

    //iframe_document = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
    //iframe_document.addEventListener('viewChange', viewChangeHandler);
    iframe_document = $("iframe").contents();
    iframe_document[0].addEventListener('viewChange', viewChangeHandler)
    //Attach delete event
    iframe_document.keydown(function(event){
        if(event.keyCode == 46) deleteElement();
    })


    //if this is the first load, get the current view from the DOM
    if (view == null) {
        view = getViewFromDOM();
    }

    setViewName();
    attachElementClicks();
    attachFileDragEvents();
    createLayerList();
    //show the options on the panel RHS
    populatePanel();

    // attachFileDragListener(); - might need to add this back in somewhere
    for (var i = 0; i < selectedElements.length; i++) {
        highlightElement(selectedElements[i]);
    }  
    for (var i = 0; i < hiddenElements.length; i++) {
        hideElements(hiddenElements[i]);
    }  
}

function setViewName() {
    document.getElementById('viewName').innerText = view;
}

function attachElementClicks() {

    $("iframe").contents().find(".view").each(function() {
        $(this).find("*").each(function() {
            this.addEventListener('click', function(event) {
                elementClickHandler(event, this.getAttribute('name'));
            }, true);
            if ($(this).prop("tagName").toLowerCase() == 'div') {
                var colour = "rgba(" + Math.round((Math.random() * 255)) + ", " + Math.round((Math.random() * 255)) + ", " + Math.round((Math.random() * 255)) + ", 0.4)";
                $(this).css("background", colour)
            }
        });
    });
}


function attachLayersClicks() {
    $("#layers li").each(function() {
        $(this).click(function(event) {
            elementClickHandler(event, this.getAttribute('id'));
        });
    });


    $("#layers li span").each(function(){
        $(this).click(function(){
            return false;
        })
        .mousedown(function(){
            isDown = true;
            triggerHide($(this));
        })
        .mouseover(function(){
            if(isDown){
                triggerHide($(this));
            };
        }); 
    });
}
function triggerHide(elem){
    if(displayClass == null){
        displayClass = (elem.hasClass("ui-icon-radio-off"))? "ui-icon-radio-off" : "ui-icon-bullet";
    }
    var thisId = elem.parent("li").attr("id")
    if(elem.hasClass(displayClass)){
        elem.toggleClass("ui-icon-bullet ui-icon-radio-off");
        hideElements(thisId);
    };
    
}


function elementClickHandler(event, name) {
    event.preventDefault();
    event.stopPropagation(); // stop it triggering click events within the iframe
    if (!event.ctrlKey) {
        for (var i = selectedElements.length - 1; i >= 0; i--) {
            unhighlightElement(selectedElements[i]);
        }
        selectedElements = [];
    }

    // Check to see if object already selected
    var index = selectedElements.indexOf(name);
    if (index < 0) {
        highlightElement(name);
        selectedElements.push(name);
    } else {
        // remove if already selected
        selectedElements.splice(index, 1);
        unhighlightElement(name);
    }
    populatePanel(event);
}

function highlightElement(name) {
    //var element = iframe_document.getElementsByName(name)[0];
    var element = iframe_document.find("#"+view+" [name='"+name+"']");
    var elemType = element.prop("tagName").toLowerCase();

    if (elemType == 'img') {
        element.css("border", "2px dashed red");
    } else {
        element.css("border", "2px dashed blue");
    }

    //element.css("zIndex", 10);

    //highlight in the layer-list
    $("#order #" + name).addClass("layerHighlight");
}

function unhighlightElement(name) {
   // var element = iframe_document.getElementsByName(name)[0];
    var element = iframe_document.find("[name='"+name+"']");
    element.css("border", "none");
    element.css("zIndex", 0);
    $("#order #" + name).removeClass("layerHighlight");
}

function hideElements(name){
    var element = iframe_document.find("#"+view+" [name='"+name+"']");
    var index = hiddenElements.indexOf(name);
    if($("#"+name+"_display").hasClass("ui-icon-radio-off")){
        if (index < 0){
            hiddenElements.push(name);   
        }
        element.css("display", "none");

    }else{
        if (index >= 0) {hiddenElements.splice(index, 1);}
        element.css("display", "block"); 
    }
}

function populatePanel() {
    var layers = ad.views[view].elements;
    for (var propertyName in layers) {
        var layer = layers[propertyName];
        if (selectedElements.length == 1 && selectedElements[0] == layer.name) {
            $("#origin button").each(function() {
                if ($(this).attr("data-origin-x") == layer.origin[0] && $(this).attr("data-origin-y") == layer.origin[1]) {
                    $(this).addClass("currentOrigin");
                } else {
                    $(this).removeClass("currentOrigin");
                }
            });
            $("#scale select").val(layer.scaleBy)

        }
    }
    group_name.value = "group_" + selectedElements[0];
}
















// SERVER REQUEST FUNCTIONS

function getAd() {
    $.ajax({
        type: "GET",
        url: '../getAd.php',
        success: updateAd
    });
}

function updateAd(json) {

    ad = JSON.parse(json);

    // if this is a single view ad, save the view. Otherwise it will be taken from the scale_controller
    if (view == null && Object.keys(ad.views).length == 1) {
        view = Object.keys(ad.views)[0];
    }

    load_iframe();

}


function deleteElement() {

    if (selectedElements.length > 0) {
        controller.className = '';
        $.ajax({
            type: "POST",
            url: '../delete.php',
            data: {
                elements: selectedElements,
                view: view
            },
            success: updateAd
        });
        selectedElements = [];
    }
}

function groupElements() {
    if (validateGroupName() && selectedElements.length > 0) {
        controller.className = '';
        $.ajax({
            type: "POST",
            url: '../group.php',
            data: {
                elements: selectedElements,
                groupName: group_name.value,
                view: view
            },
            success: updateAd
        });
        //Add new group to selected elementsz
        selectedElements = [group_name.value];
    }
}

function validateGroupName() {
    clearTimeout(reset);
    var val = $("#group_name").val();
    var newName = iframe_document.find("#"+view+" [name="+val+"]");
    if (newName.length > 0) {
        $("#group_name").addClass("invalid");
        var reset = setTimeout(function() {
            $("#group_name").removeClass("invalid");
        }, 2000)
        return false;
    } else {
        $("#group_name").removeClass("invalid");
        return true;
    }
}


function breakGroup() {
    if (selectedElements.length > 0) {
        controller.className = '';
        $.ajax({
            type: "POST",
            url: '../break.php',
            data: {
                elements: selectedElements,
                view: view
            },
            success: updateAd
        });
        selectedElements = [];
    }
}

function changeOrigin(event) {
    if (selectedElements.length > 0) {
        var newOrigin = [event.target.getAttribute("data-origin-x"), event.target.getAttribute("data-origin-y")];
        controller.className = '';
        $.ajax({
            type: "POST",
            url: '../changeOrigin.php',
            data: {
                elements: selectedElements,
                origin: newOrigin,
                view: view
            },
            success: updateAd
        });
    }
}


function orderElement(event, ui) {
    var elementNames = $("#layers").sortable('toArray');
    $.ajax({
        type: "POST",
        url: '../order.php',
        data: {
            elementNames: elementNames,
            view: view
        },
        success: updateAd
    });
}

function scaleElement(event) {

    if (selectedElements.length > 0) {
        var ddl = event.target;
        var scale = ddl.options[ddl.selectedIndex].value;
        $.ajax({
            type: "POST",
            url: '../scale.php',
            data: {
                elements: selectedElements,
                scaleBy: scale,
                view: view
            },
            success: updateAd
        });
    }
}

function uploadFile(e) {
    // preventing the duplicate submissions if the current one is in progress
    if (fileForm.hasClass('is-uploading')) return false;

    if (droppedFiles.length >= 1) {

        $(".box_file").attr("name", view + "_files[]");

        fileForm.addClass('is-uploading').removeClass('is-error');

        // gathering the form data
        var ajaxData = new FormData(fileForm.get(0));

        $.each(droppedFiles, function(i, file) {
            ajaxData.append(fileInput.attr('name'), file);
        });

        ajaxData.append('dropped', 'true');        
        ajaxData.append('view', view);

        // ajax request
        $.ajax({
            url: '../update.php',
            type: 'POST',
            data: ajaxData,
            contentType: false,
            processData: false,
            complete: function() {
            },
            success: function(data) {
                if (!data.success) errorMsg.text(data.error);
                updateAd(data);
            },
            error: function(data) {
                fileForm.addClass('is-error')
                errorMsg.append(data.responseText);
            }
        });
    }
};
