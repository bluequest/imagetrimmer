<?php
    
    // If the user has chosen to clear the creative we treat it as a brand new build. Otherwise we are just updating.
    if (isset($_POST['new_creative']) || !file_exists('creative/ad.json')) {
        require_once("create.php");
    } else {
        require_once("update.php");
    }
?>
<script>
window.location = 'preview/';
</script>