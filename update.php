<?php
	
    require_once("autoload_register.php");
    
    // get the json from file
    $json = file_get_contents('creative/ad.json');
    $oJSON = json_decode($json);

    // Create the Ad Object
    $bq_ad = BQ_Ad::contructFromObject($oJSON);

    //Check to see that the user has selected the same format as the existing ad
    if(isset($_POST['format']) && $_POST['format'] == $bq_ad->id || isset($_POST['dropped']) && $_POST['dropped'] == true && isset($_POST['view'])) { 
    	$bq_ad->update($_FILES, $_POST['view']);    	
	    $new_json = json_encode($bq_ad);
	    file_put_contents ("creative/ad.json" , $new_json);

        echo $new_json;

    } else {
    	// echo "<p>Wrong format selected, please select the same format and platform as the existing ad. ({$bq_ad->platform_dir} - {$bq_ad->name})";
    }    

?>