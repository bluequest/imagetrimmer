<!DOCTYPE html>
<html>

<head>
    <title>adBase</title>
    <script type="text/javascript">

    var ads;

    document.addEventListener("DOMContentLoaded", function() {

        formats = JSON.parse(document.getElementById("jsonData").value).formats;

        document.getElementById("format").addEventListener("change", function() {

            var format;

            for (var i = formats.length - 1; i >= 0; i--) {
                if (formats[i].id == this.value) {
                    format = formats[i];
                    break;
                }
            }

            drawViews(format);
            drawPlatforms(format);
            
        });
    });

    function drawViews(format) {
        var formatsText = '';

        for (var i = 0; i < format.views.length; i++) {
            formatsText += "<div id='" + format.views[i].id + "' class='view'>" +        
                "<p>" + format.views[i].name + "</p>";          

            formatsText += "<input type='file' name='" + format.views[i].name + "_PSD'>" +       
            "</div>";
        }

        document.getElementById("views").innerHTML = formatsText;

    }

    function drawPlatforms(format) {

        var platformList;

        for (var i = 0; i < format.platforms.length; i++) {
            platformList += "<option value='" + format.platforms[i].id + "'>" + format.platforms[i].name + "</option>";
        }

        document.getElementById("platform").innerHTML = platformList;

    }

    </script>
</head>

<body>
    <form action="form_handler.php" method="post" enctype="multipart/form-data">
        <h3>Process Images</h3>

        <!-- Formats -->
        <h4>
			<span>Select Format: </span>
			<select name="format" id="format">
                <option value='0'>...choose a platform</option>
<?php
    // get the json from file
    $json = file_get_contents('adBuilder.json');
    $formats = json_decode($json, true)['formats']; // get the formats object from inside the json decode. this is what we really want

    // Create an input option for each ad
    for ($i=0; $i < count($formats); $i++) { 
        echo "<option value='{$formats[$i]['id']}'>{$formats[$i]['name']}</option>";
    }
?>		
			</select>
		</h4>
        
        <!-- Views -->
        <h3>Views</h3>
        <p>Upload the images you would like to be seen in each view.</p>
        <div id="views"></div>

        <!-- Platforms -->
        <h3>Platforms</h3>
        <h4>
			<span>Select Platform: </span>
			<select name="platform" id="platform"></select>			
		</h4>

        <h4>
            <label for="new_creative">New Creative</label>
            <input name="new_creative" id="new_creative" type="checkbox"></input>            
        </h4>

        <input type="submit" name="submit" value="Create"></input>

<?php
    echo "<input type='hidden' id='jsonData' value='{$json}' />";
?>          

    </form>
</body>

</html>
