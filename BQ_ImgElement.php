<?php

class BQ_ImgElement extends Bq_Element {

    var $uploaded_image;
    var $image;
    var $platform;
    var $ext;

    function __construct($uploaded_image, $platform) {

        $this->type = 'image';
        $this->uploaded_image = $uploaded_image; // save the orignal image
        $this->platform = $platform;

    }

    public static function constructFromUpload($uploaded_image, $container, $platform, $fromPSD) {

        $instance = new static($uploaded_image, $platform);

        if($fromPSD == true) {
            $instance->setNamePSD(); // save the name and extention of the object
            $instance->processPSDLayer($container); // cut the image and save the result. Get the resultant image dimensions and coordinates
        } else {
            $instance->setNameImg(); // save the name and extention of the object
            $instance->processImage($container); // cut the image and save the result. Get the resultant image dimensions and coordinates
        }

        $instance->saveImage($_SERVER['DOCUMENT_ROOT'] . "creative", $container);

        $instance->createHTML($container);
        $instance->createCSS($container);

        return $instance;

    }

    public static function constructFromObject($element) {

        $instance = new static($element->uploaded_image, $element->platform);
        
        $instance->name = $element->name;
        $instance->ext = $element->ext;

        $instance->top = $element->top;
        $instance->left = $element->left;

        $instance->right = $element->right;
        $instance->bottom = $element->bottom;

        $instance->width = $element->width;
        $instance->height = $element->height;

        $instance->origin = $element->origin;
        $instance->scaleBy = $element->scaleBy;


        $instance->css = $element->css;
        $instance->html = $element->html;

        return $instance;
        
    }

    function setNamePSD() {

        $label = $this->uploaded_image->getImageProperty('label');
        $this->setName($label);

    }

    function setNameImg() {

        $label = explode('.',  $this->uploaded_image['name'])[0];
        $this->setName($label);

    }

    function setName($label) {

        // get the name
        $this->name = str_replace("_jpg", "", $label); 

        // get the extension
        //check to see whether this is a jpg or a png
        if (strpos($label, '_jpg') !== false) {
            $this->ext = 'jpg';
        } else {
            $this->ext = 'png';            
        }  

    }

    function processPSDLayer($container) {

        $img = $this->uploaded_image;

        //save the new image
        $this->image = $img;

        $image_page = $img->getImagePage();

        //save the image offsets and dimensions
        $this->top = $image_page['y'];
        $this->left = $image_page['x'];
        $this->right = $container->width - $image_page['x'] - $image_page['width'];
        $this->bottom = $container->height - $image_page['y'] - $image_page['height'];

        $this->width = $image_page['width'];
        $this->height = $image_page['height'];

    }

    function processImage($container) {
        $img = new Imagick($this->uploaded_image["tmp_name"]);

        if($container->width == 0) {
            $container->width = $img->getImageWidth();
        }
        if($container->height == 0) {
            $container->height = $img->getImageHeight();
        }

        $newImg = new Imagick();
        $newImg->newImage($img->getImageWidth() + 2, $img->getImageHeight() + 2, new ImagickPixel('transparent'));
        $newImg->compositeImage($img, Imagick::COMPOSITE_COPY , 1, 1);
        $newImg->trimImage(0);

        //save the new image
        $this->image = $newImg;

        $image_page = $newImg->getImagePage();

        //save the image offsets and dimensions
        $this->top = $image_page['y'] - 1;
        $this->left = $image_page['x'] - 1;

        $this->right = $image_page['width'] - $image_page['x'] - $newImg->getImageWidth() - 1;
        $this->bottom = $image_page['height'] - $image_page['y'] - $newImg->getImageHeight() - 1;

        $this->width = $newImg->getImageWidth();
        $this->height = $newImg->getImageHeight();

    }

    public function saveImage($dir, $container) {

        // get the directory string, checking if the platform requires a flat file structure
        $fullDir = "{$dir}/" . ($this->platform->flat ? "" : "images/{$container->name}/") . "{$this->name}.{$this->ext}";

        //create the directory if it does not already exist
        if (!file_exists(dirname($fullDir))) {
            mkdir(dirname($fullDir), 0777, true);
        }

        // save the image
        if(!$this->image->writeImage("{$fullDir}")) {
            echo "Unable to save image";
        }
    }

    function createHTML($container) {
        $this->html = "<img "
            . ($container->crossDevice ? 'data-src' : 'src') . "='".($this->platform->flat ? "" : "images/{$container->name}/")."{$this->name}.{$this->ext}'"
            ." name='{$this->name}' class='{$this->name}" . ($container->responsive ? ' ' . $this->origin[0] . $this->origin[1] : '') . "'"
            .($container->responsive ? " data-scale='{$this->scaleBy}'" : "")
            ." />";
    }

}

?>