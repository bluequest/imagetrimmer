<?php

abstract class BQ_Element {

    var $type;

    var $top;
    var $left;
    var $right;
    var $bottom;
    var $width;
    var $height;

    var $origin = ["c","c"];
    var $scaleBy = 'min';

    var $name;

    var $css;
    var $html;

    function createCSS($container) {
        if ($container->responsive) {
            $this->createCSS_Responsive($container);
        } else {
            $this->createCSS_Static();
        }
    }

    function createCSS_Static() {
        $this->css = "left: {$this->left}px; top: {$this->top}px; width: {$this->width}px; height: {$this->height}px;";
    }

    function createCSS_Responsive($container) {

        //set widths
        $css = "width: " . $this->width . "px; height: " . $this->height . "px;";

        //set position
        if($this->origin[0] == "l"){
            $css = $css . " left: " . $this->left / (0.01 * $container->width) . "%;";
            if($this->origin[1] == "t") {
                $css = $css . " top: " . $this->top / (0.01 * $container->height) . "%;";
            } else if ($this->origin[1] == "b") {
                $css = $css . " bottom: " . $this->bottom / (0.01 * $container->height) . "%;";
            } else {
                $css = $css . " top: " . ((2 * $this->top) + $this->height) / (0.02 * $container->height) . "%;";
               // $css = $css . $this->crossBrowserPrefix("transform: translateY(-50%);");
            }
        } else if ($this->origin[0] == "r") {
            $css = $css . " right: " . $this->right / (0.01 * $container->width) . "%;";
            if($this->origin[1] == "t") {
                $css = $css . " top: " . $this->top / (0.01 * $container->height) . "%;";
            } else if ($this->origin[1] == "b") {
                $css = $css . " bottom: " . $this->bottom / (0.01 * $container->height) . "%;";
            } else {
                $css = $css . " top: " . ((2 * $this->top) + $this->height) / (0.02 * $container->height) . "%;";
               // $css = $css . $this->crossBrowserPrefix("transform: translateY(-50%);");
            }
        } else if ($this->origin[1] == "t") {
            $css = $css . " left: " . ((2 * $this->left) + $this->width) / (0.02 * $container->width) . "%;";
            $css = $css . " top: " . $this->top / (0.01 * $container->height) . "%;";
            //$css = $css . $this->crossBrowserPrefix("transform: translateX(-50%);");
        } else if ($this->origin[1] == "b") {
            $css = $css . " left: " . ((2 * $this->left) + $this->width) / (0.02 * $container->width) . "%;";
            $css = $css . " bottom: " . $this->bottom / (0.01 * $container->height) . "%;";
           // $css = $css . $this->crossBrowserPrefix("transform: translateX(-50%);");
        } else {          
            $css = $css . " left: " . ((2 * $this->left) + $this->width) / (0.02 * $container->width) . "%;";
            $css = $css . " top: " . ((2 * $this->top) + $this->height) / (0.02 * $container->height) . "%;";
           // $css = $css . $this->crossBrowserPrefix("transform: translate(-50%, -50%);");
        }

        $this->css = $css;

    }

    function crossBrowserPrefix($css) {

        $prefixCSS = '';

        $prefixi = ['-o-', '-moz-', '-webkit-', '-ms-', ''];
        foreach ($prefixi as $prefix) {
            $prefixCSS = $prefixCSS . ' ' . $prefix . $css;
        }

        return $prefixCSS;
    }

}

?>