<?php

    if(isset($_POST['elementNames']) && isset($_POST['view'])) {

        require_once("autoload_register.php");
        
        // get the json from file
        $json = file_get_contents('creative/ad.json');
        $oJSON = json_decode($json);

        // Create the Ad Object
        $bq_ad = BQ_Ad::contructFromObject($oJSON);

        $bq_ad->moveElement($_POST['elementNames'], $_POST['view']);

         // Save the object as a JSON file
        $new_json = json_encode($bq_ad);
        file_put_contents ("creative/ad.json" , $new_json);

        echo $new_json;

    } else {
        echo "FAILURE";
    }




?>