<?php

require_once("autoload_register.php");

if($_POST) {    

    clearCreative('creative'); 
    
    // get the json from file
    $json = file_get_contents('adBuilder.json');
    $formats = json_decode($json)->formats; // get the formats object from inside the json decode. this is what we really want

    // Create the Ad Object
    $bq_ad = createAdObject($formats, $_POST, $_FILES);
  
    // Save the object as a JSON file
    $new_json = json_encode($bq_ad);
    file_put_contents ("creative/ad.json" , $new_json);

}

function clearCreative($dir) {

    $files = glob("{$dir}/*"); // get all file names
    foreach ($files as $file) {
        if (is_dir($file)) {
            clearCreative($file);
        } else {
            unlink($file);
        }
    }

    // Delete the directory, unless it's the creative directory
    if($dir != 'creative') {
        rmdir($dir);
    }

}

function createAdObject ($formats, $post, $files) {
    foreach ($formats as $format) {
        if ($format->id == $post['format']) {
            return BQ_Ad::constructFromUpload($format, $post, $files);
        }
    }
}

?>